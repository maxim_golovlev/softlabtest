//
//  MainApi.swift
//  SoftlabTest
//
//  Created by Admin on 03.07.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import Alamofire

protocol MainApi {
    static func sendRequest(type: HTTPMethod, url: String!, parameters: [String: AnyObject]?, completion: ServerArrayResult?)
}

extension MainApi {
    
    static func sendRequest(type: HTTPMethod, url: String!, parameters: [String: AnyObject]?, completion: ServerArrayResult?) {
        
        URLCache.shared.removeAllCachedResponses()
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            switch response.result {
            case .success(let value):
                guard let response = value as? [Dictionary<String, AnyObject>] else {
                    completion?( .error (message: NSLocalizedString("network_error", comment: "")))
                    return
                }
                completion?( .success (response: response))
                
            case .failure(let error):
                completion?( .error (message: error.localizedDescription))
            }
        }
    }
}
