
import Foundation
import UIKit

protocol StoryboardSceneType {
    static var storyboardName: String { get }
    static var controllerName: String { get }
}

extension StoryboardSceneType {
    static func storyboard() -> UIStoryboard {
        return UIStoryboard(name: self.storyboardName, bundle: nil)
    }
    
    static func initialViewController() -> UIViewController {
        guard let vc = storyboard().instantiateInitialViewController() else {
            fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
        }
        return vc
    }
    
    static func instantiateViewController() -> UIViewController {
        let vc = storyboard().instantiateViewController(withIdentifier: controllerName)
        return vc
    }
}

struct StoryboardScene {
    
    private init() {}
    
    struct CategoryFeed: StoryboardSceneType {
        static var storyboardName = "Main"
        static var controllerName = "Feed"
    }
}
