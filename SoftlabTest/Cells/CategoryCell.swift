//
//  CategoryCell.swift
//  SoftlabTest
//
//  Created by Admin on 03.07.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import TableKit

class CategoryCell: UITableViewCell, ConfigurableCell {
    
    static var defaultHeight: CGFloat? {
        return 44
    }

    func configure(with data:(title: String?, subs: Bool)) {
        textLabel?.text = data.title
        accessoryType = data.subs ? .disclosureIndicator : .none
    }
    
}
