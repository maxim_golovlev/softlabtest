//
//  Category.swift
//  SoftlabTest
//
//  Created by Admin on 03.07.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import ObjectMapper_Realm

class FeedCategory: Object, Mappable {
    
    @objc dynamic var id = 0
    @objc dynamic var title: String?
    var subs = List<FeedCategory>()

    required convenience init?(map: Map) {
        self.init()
    }
    
    override class func primaryKey() -> String? {
        return "title"
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        subs <- (map["subs"], ListTransform<FeedCategory>())
    }
}



