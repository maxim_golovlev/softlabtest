import Foundation

enum APIArrayResponse {
    case success (response: [Dictionary<String, AnyObject>])
    case error (message: String?)
}

typealias ServerArrayResult = (_ response: APIArrayResponse) -> Void

enum ResponseError: Error {
    case withMessage(String?)
}
