//
//  CategoryViewController.swift
//  SoftlabTest
//
//  Created by Admin on 03.07.2018.
//  Copyright (c) 2018 Admin. All rights reserved.
//

import UIKit
import TableKit

protocol CategoryViewProtocol: BaseView {
    var presenter:CategoryPresenterProtocol { get set }
}

class CategoryViewController: UIViewController, CategoryViewProtocol {
    
    // MARK: - Public properties
    
    lazy var presenter:CategoryPresenterProtocol = CategoryPresenter(view: self, categories: nil)
    
    // MARK: - Private properties
    
    var tableDirector: TableDirector!
    
    lazy var refreshControl: UIRefreshControl = {
        let rc = UIRefreshControl()
        rc.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        return rc
    }()
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableDirector = TableDirector.init(tableView: tableView)
            tableView.tableFooterView = UIView()
            tableView.addSubview(refreshControl)
        }
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.fetchCategories() { cat in
            self.updateTableView(category: cat)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: - Display logic
    
    // MARK: - Actions
    
    @objc func handleRefresh() {
        
        presenter.fetchCategories() { cat in
            self.updateTableView(category: cat)
        }
    }
    
    // MARK: - Overrides
    
    // MARK: - Private functions
    
    func updateTableView(category: [FeedCategory]) {
        
        refreshControl.endRefreshing()
        
        tableDirector.clear()
        
        let section = TableSection()
        
        let rows = category.flatMap({ TableRow<CategoryCell>.init(item: ($0.title, $0.subs.count > 0)) })
        
        rows.forEach { (row) in
            row.on(.click) { [unowned self] options in
                
                let index = options.indexPath.row
                let selectedCategory = category[index]
                
                if selectedCategory.subs.count > 0 {
                    
                    if let vc = StoryboardScene.CategoryFeed.instantiateViewController() as? CategoryViewProtocol {
                        
                        vc.presenter = CategoryPresenter.init(view: vc, categories: Array(selectedCategory.subs))
                        
                        self.navigationController?.pushViewController(vc as! UIViewController, animated: true)
                    }
                }
            }
        }
        
        section.append(rows: rows)
        
        tableDirector.append(section: section)
        
        tableDirector.reload()
    }
}
