//
//  NewsManager.swift
//  SoftlabTest
//
//  Created by Admin on 03.07.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import RealmSwift

class CategoryManager: MainApi {

    private enum Urls: String {
        case categories = "https://money.yandex.ru/api/categories-list"
    }

    static func fetchNews() -> Promise<[FeedCategory]> {
        
        return Promise.init(resolvers: { (fullfill, reject) in
            
            sendRequest(type: .get, url: Urls.categories.rawValue, parameters: nil, completion: { (response) in
                switch response {
                case let .success(response: json):
                    
                    let categories = json.flatMap({ Mapper<FeedCategory>().map(JSON: $0) })

                    let realm = try! Realm()
                    
                    try! realm.write {
                        categories.forEach({ (article) in
                            realm.add(article, update: true)
                        })
                    }
                    
                    fullfill(categories)
                        
                case let .error(message: msg):
                    reject(ResponseError.withMessage(msg))
                }
            })
            
        })
    }
    
}
