//
//  SoftlabTestTests.swift
//  SoftlabTestTests
//
//  Created by Admin on 05.07.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import XCTest
@testable import SoftlabTest
import RealmSwift
import ObjectMapper

class SoftlabTestTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testCategoryPresenter() {
        
        let json = ["title":"Телефон","subs":[["id":157291,"title":"Мобильная связь"],["id":524624,"title":"Городской телефон"],["id":157298,"title":"IP-телефония"]]] as [String : Any]

        if let category = Mapper<FeedCategory>().map(JSON: json) {
        
            let categoryPresenter = CategoryPresenter.init(view: nil, categories: [category])
            
            categoryPresenter.fetchCategories() { cat in
                XCTAssertEqual(cat.first?.subs.first?.title, "Мобильная связь")
            }
        } else {
            XCTFail()
        }
    }
}
