//
//  CategoryPresenter.swift
//  SoftlabTest
//
//  Created by Admin on 03.07.2018.
//  Copyright (c) 2018 Admin. All rights reserved.
//

import UIKit
import RealmSwift

protocol CategoryPresenterProtocol: class {
    init(view: CategoryViewProtocol?, categories: [FeedCategory]?)
    func fetchCategories(completion: @escaping ([FeedCategory]) -> ())
}

class CategoryPresenter {
    
    // MARK: - Public variables
    weak var view:CategoryViewProtocol?
    
    var categories: [FeedCategory]?
    
    // MARK: - Private variables
    private var realm = try! Realm()
    
    required init(view: CategoryViewProtocol?, categories: [FeedCategory]?) {
        self.categories = categories
    }
}

extension CategoryPresenter: CategoryPresenterProtocol {
    func fetchCategories(completion: @escaping ([FeedCategory]) -> ()){
        
        if let cat = categories {
            completion(cat)
            return
        }
        
        self.view?.startLoading()
        
        CategoryManager.fetchNews()
            .then { (categories) -> Void in
                completion(categories)
            }
            .always {
                self.view?.stopLoading()
            }
            .catch { (error) in
                if case let ResponseError.withMessage(msg) = error {
                    self.view?.showAlert(title: nil, message: msg)
                    
                    let savedCategories = self.getFirstLevelCategories()
                    completion(savedCategories)
                }
        }
    }
    
    func getFirstLevelCategories() -> [FeedCategory] {
        return Array(self.realm.objects(FeedCategory.self).filter("id == 0"))
    }
}
